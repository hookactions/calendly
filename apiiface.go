package calendly

type IApi interface {
	Echo() (*EchoResponse, error)
	CreateHook(input CreateHookInput) (*CreateHookResponse, error)
	GetHook(input GetHookInput) (*GetHookResponse, error)
	GetHooks() (*GetHooksResponse, error)
	DeleteHook(input DeleteHookInput) (*DeleteHookResponse, error)
	Me() (*MeResponse, error)
	GetEventTypes(input *GetEventTypesInput) (*EventTypesResponse, error)
}
