test:
	go test -v ./... -cover -race
	go vet

	go run honnef.co/go/tools/cmd/staticcheck ./...
